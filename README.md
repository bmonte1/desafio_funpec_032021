# Desafios Técnicos - 101 Programador FrontEnd / FUNPEC

## Sobre
Cada questão ficou dividida em uma pasta (Q1, Q2, Q3, Q4). É possível conferir cada questão na sua respectiva página no arquivo index.html, a única diferença é a qustão 4 que possui dois arquivos: css.html e javascript.html, cada um representa uma implementação diferente.

### Descrição dos desafios
- **Desafio 1 -** Utilizando javascript puro, produza uma classe "ClasseJS" com uma implementação que realize a substituição do trecho "{{ mensagem }}" do HTML pela mensagem "Olá Mundo!". 
- **Desafio 2-** Usando a biblioteca javascript "async.series" crie um script que receba uma coleção de funções "tarefas" e uma função "retorno". O seu código deverá rodar as funções na coleção "tarefas", uma por vez, cada uma rodando uma vez que a anterior tenha completado. Se alguma das funções passar um erro para o seu callback, nenhuma outra função irá rodar e "retorno" é chamado imediatamente com o valor do erro. Senão, "retorno" recebe um array de resultados quando "tarefas" completar. 
- **Desafio 3 -** Implemente um botão alinhado no centro da tela (vertical e horizontalmente) que possa ser arrastado. Caso se aproxime das bordas do navegador, a cor do fundo deve mudar de branco para vermelho. 
- **Desafio 4 -** Implemente uma página com um elemento HTML e que realize uma animação de deslizar para o lado oposto da tela sempre que o elemento é clicado (a) utilizando CSS e (b) utilizando Javascript (cada implementação deverá gerar uma página HTML separada). 
